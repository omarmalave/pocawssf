<?php

require 'vendor/autoload.php';


/**
* 
*/
class AwsSF
{

	protected $executionData;
	protected $awsKey = 'AKIAIBRRB2EWYPMSMEAQ';
	protected $awsSecret = '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';
	protected $awsRegion = 'us-east-1';
	protected $awsVersion = 'latest';

	function __construct() {
		
		session_start();

		$this->SfnClient = new Aws\Sfn\SfnClient([
			'region'   => $this->awsRegion,    
			'version'  => $this->awsVersion,
			'credentials' => [
				'key'    => $this->awsKey,
				'secret' => $this->awsSecret,
			],    
		]);

		$this->Sec2Client = new Aws\Ec2\Ec2Client([
			'region'   => $this->awsRegion,    
			'version'  => $this->awsVersion,
			'credentials' => [
				'key'    => $this->awsKey,
				'secret' => $this->awsSecret,
			],    
		]);

	}

	public function newSmExecution() {

		$taskName = md5(date('Y-m-s H:i:s'));

		try {

			$_SESSION["execution"] = $this->SfnClient->StartExecution([
				'input' => '{"msg": "Ok"}',
				'name' => $taskName,
				'stateMachineArn' => "arn:aws:states:us-east-1:410586708178:stateMachine:createEc2Instance"
			]);

		} catch (Exception $e) {
			echo "Exception: ".$e;
		}
	}

	public function runTask() {

		//Get Activity Task Result
		$getActivityTaskResult = $this->SfnClient->getActivityTask([
		    'activityArn' => 'arn:aws:states:us-east-1:410586708178:activity:createEc2Instance', // REQUIRED
		    'workerName'  => 'pocWorker',
		]);

		$input = $getActivityTaskResult["input"];
		$token = $getActivityTaskResult["taskToken"];

		$inputStr = json_decode($input);

		if ($getActivityTaskResult != null) {
			
			try {

				$this->SfnClient->sendTaskSuccess([
				    'output'    => $this->createEc2Instance(), // REQUIRED
				    'taskToken' => $token, // REQUIRED
				]);

			} catch (Exception $e) {

				$this->SfnClient->sendTaskFailure([
				    'cause' => $e->getMessage(),
				    'error' => 'POC-0001',
				    'taskToken' => $token, // REQUIRED
				]);

			}

		} 
	}

	public function stopSmExecution() {

		if (!$_SESSION["execution"]['executionArn']) {
			
			echo "Bad Request";
		}

		try {

			$this->SfnClient->stopExecution([
			    'cause' => 'End of Task',
			    'error' => 'POC-0002',
			    'executionArn' => $_SESSION["execution"]['executionArn'], // REQUIRED
			]);


		} catch (Exception $e) {
			
		}
	}

	protected function createEc2Instance() {

		$keyPairName = 'my-keypair';
		$result = $this->Sec2Client->createKeyPair(array(
		    'KeyName' => $keyPairName
		));


		// Save the private key
		$saveKeyLocation = getenv('HOME') . "./ssh/{$keyPairName}.pem";
		file_put_contents($saveKeyLocation, $result['keyMaterial']);

		// Update the key's permissions so it can be used with SSH
		chmod($saveKeyLocation, 0600);


		// Create the security group
		$securityGroupName = 'my-security-group';
		$result = $this->Sec2Client->createSecurityGroup(array(
		    'GroupName'   => $securityGroupName,
		    'Description' => 'Basic web server security'
		));

		// Get the security group ID (optional)
		$securityGroupId = $result->get('GroupId');

		// Set ingress rules for the security group
		$this->Sec2Client->authorizeSecurityGroupIngress(array(
		    'GroupName'     => $securityGroupName,
		    'IpPermissions' => array(
		        array(
		            'IpProtocol' => 'tcp',
		            'FromPort'   => 80,
		            'ToPort'     => 80,
		            'IpRanges'   => array(
		                array('CidrIp' => '0.0.0.0/0')
		            ),
		        ),
		        array(
		            'IpProtocol' => 'tcp',
		            'FromPort'   => 22,
		            'ToPort'     => 22,
		            'IpRanges'   => array(
		                array('CidrIp' => '0.0.0.0/0')
		            ),
		        )
		    )
		));

		// Launch an instance with the key pair and security group
		$result = $this->Sec2Client->runInstances(array(
		    'ImageId'        => 'ami-570f603e',
		    'MinCount'       => 1,
		    'MaxCount'       => 1,
		    'InstanceType'   => 'm1.small',
		    'KeyName'        => $keyPairName,
		    'SecurityGroups' => array($securityGroupName),
		));

		$instanceIds = $result->getPath('Instances/*/InstanceId');

		return '{"msg": "Ok"}';
	}


}

?>