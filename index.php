<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PoC AWS Steps Functions</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      
      body { 
        margin-top: 30px
       }

    </style>
  </head>
  <body>

    <div class="container">
      
      <div class="row">
        
        <div class="col-lg-12">
        
        <legend>PoC AWS Steps Functions</legend>

          <div class="btnWrapper">

            <button type="button" class="btn btn-primary" id="newExecutionBtn">
              <span class="glyphicon glyphicon-flash" aria-hidden="true"></span>
              New Execution
            </button>


            <button type="button" class="btn btn-warning" id="stopExecutionBtn" style="display: none;">
              <span class="glyphicon glyphicon-flash" aria-hidden="true"></span>
              Stop Execution
            </button>

            <button type="button" class="btn btn-success" id="runTaskBtn" style="display: none;">
              <span class="glyphicon glyphicon-flash" aria-hidden="true"></span>
              Run Task
            </button>

          </div>
        </div>

      </div>


    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <script type="text/javascript">
      
      $('document').ready(function(){

        $('#newExecutionBtn').click(function(){

          $('#newExecutionBtn').prop('disabled', true);

          $.get( "route.php?task=newExec", function( data ) {

            $('#newExecutionBtn').hide();
            $('#stopExecutionBtn').show();
            $('#runTaskBtn').show();

          });
        });


        $('#stopExecutionBtn').click(function(){

          $('#stopExecutionBtn').prop('disabled', true);

          $.get( "route.php?task=stopExec", function( data ) {


            $('#newExecutionBtn').prop('disabled', false);
            $('#stopExecutionBtn').hide();
            $('#runTaskBtn').hide();            
            $('#newExecutionBtn').show();            
          });
        });



        $('#runTaskBtn').click(function(){

          $('#runTaskBtn').prop('disabled', true);

          $.get( "route.php?task=runTask", function( data ) {

            alert('done '+data);
            $('#newExecutionBtn').prop('disabled', false);
            $('#stopExecutionBtn').hide();
            $('#runTaskBtn').hide();            
            $('#newExecutionBtn').show();            
          });
        });



      });
    </script>


  </body>
</html>